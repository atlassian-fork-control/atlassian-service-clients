export const sampleAddonProductEntriesResponse = {
  "sys": {
    "type": "Array"
  },
  "total": 2,
  "skip": 0,
  "limit": 50,
  "items": [
    {
      "fields": {
        "key": "license.pricingplan.com.hgh.explainer-for-jira",
        "defaultStdPricingPlanMonthly": null,
        "defaultStdPricingPlanYearly": {
          "sys": {
            "space": {
              "sys": {
                "type": "Link",
                "linkType": "Space",
                "id": "3s3v3nq72la0"
              }
            },
            "id": "9KKNU69Se5U58dv561jrE",
            "type": "Entry",
            "createdAt": "2019-07-01T02:24:18.118Z",
            "updatedAt": "2019-07-09T12:38:41.535Z",
            "environment": {
              "sys": {
                "id": "stg",
                "type": "Link",
                "linkType": "Environment"
              }
            },
            "revision": 3,
            "contentType": {
              "sys": {
                "type": "Link",
                "linkType": "ContentType",
                "id": "pricingPlan"
              }
            },
            "locale": "en-US"
          },
          "fields": {
            "status": "ACTIVE",
            "configuration": [
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 1140
                  },
                  {
                    "currency": "USD",
                    "unit_price": 10
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 0,
                "unit_limit_upper": 10
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 5560
                  },
                  {
                    "currency": "USD",
                    "unit_price": 49
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 11,
                "unit_limit_upper": 25
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 16900
                  },
                  {
                    "currency": "USD",
                    "unit_price": 149
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 26,
                "unit_limit_upper": 50
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 28300
                  },
                  {
                    "currency": "USD",
                    "unit_price": 249
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 51,
                "unit_limit_upper": 100
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 39600
                  },
                  {
                    "currency": "USD",
                    "unit_price": 349
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 101,
                "unit_limit_upper": 250
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 50900
                  },
                  {
                    "currency": "USD",
                    "unit_price": 449
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 251,
                "unit_limit_upper": 500
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 113000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 990
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 501,
                "unit_limit_upper": 2000
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 226000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 1990
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 2001,
                "unit_limit_upper": 10000
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 339000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 2990
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 10001,
                "unit_limit_upper": null
              }
            ],
            "pricingPlanId": "c77e96f3-e9f6-43ac-8d15-8b92f4c6451e",
            "sourceSequence": null,
            "source": "hams",
            "type": "Standard"
          }
        },
        "defaultPremiumPricingPlanMonthly": null,
        "defaultPremiumPricingPlanYearly": null,
        "defaultFreePricingPlanMonthly": null,
        "description": "Explainer for JIRA"
      },
      "sys": {
        "space": {
          "sys": {
            "type": "Link",
            "linkType": "Space",
            "id": "3s3v3nq72la0"
          }
        },
        "id": "WiJUHC1sTvANZS1kLZryv",
        "type": "Entry",
        "createdAt": "2019-06-10T22:49:55.714Z",
        "updatedAt": "2019-07-09T12:38:41.396Z",
        "environment": {
          "sys": {
            "id": "stg",
            "type": "Link",
            "linkType": "Environment"
          }
        },
        "revision": 5,
        "contentType": {
          "sys": {
            "type": "Link",
            "linkType": "ContentType",
            "id": "product"
          }
        },
        "locale": "en-US"
      }
    },
    {
      "fields": {
        "key": "pricingplan.com.hgh.explainer-for-jira",
        "defaultStdPricingPlanMonthly": null,
        "defaultStdPricingPlanYearly": {
          "sys": {
            "space": {
              "sys": {
                "type": "Link",
                "linkType": "Space",
                "id": "3s3v3nq72la0"
              }
            },
            "id": "9KKNU69Se5U58dv561jrE",
            "type": "Entry",
            "createdAt": "2019-07-01T02:24:18.118Z",
            "updatedAt": "2019-07-09T12:38:41.535Z",
            "environment": {
              "sys": {
                "id": "stg",
                "type": "Link",
                "linkType": "Environment"
              }
            },
            "revision": 3,
            "contentType": {
              "sys": {
                "type": "Link",
                "linkType": "ContentType",
                "id": "pricingPlan"
              }
            },
            "locale": "en-US"
          },
          "fields": {
            "status": "ACTIVE",
            "configuration": [
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 1140
                  },
                  {
                    "currency": "USD",
                    "unit_price": 10
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 0,
                "unit_limit_upper": 10
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 5560
                  },
                  {
                    "currency": "USD",
                    "unit_price": 49
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 11,
                "unit_limit_upper": 25
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 16900
                  },
                  {
                    "currency": "USD",
                    "unit_price": 149
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 26,
                "unit_limit_upper": 50
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 28300
                  },
                  {
                    "currency": "USD",
                    "unit_price": 249
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 51,
                "unit_limit_upper": 100
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 39600
                  },
                  {
                    "currency": "USD",
                    "unit_price": 349
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 101,
                "unit_limit_upper": 250
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 50900
                  },
                  {
                    "currency": "USD",
                    "unit_price": 449
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 251,
                "unit_limit_upper": 500
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 113000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 990
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 501,
                "unit_limit_upper": 2000
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 226000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 1990
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 2001,
                "unit_limit_upper": 10000
              },
              {
                "prices": [
                  {
                    "currency": "JPY",
                    "unit_price": 339000
                  },
                  {
                    "currency": "USD",
                    "unit_price": 2990
                  }
                ],
                "starter": false,
                "pricing_strategy": "FIXED_COST",
                "unit_description": "user",
                "unit_limit_lower": 10001,
                "unit_limit_upper": null
              }
            ],
            "pricingPlanId": "c77e96f3-e9f6-43ac-8d15-8b92f4c6451e",
            "sourceSequence": null,
            "source": "hams",
            "type": "Standard"
          }
        },
        "defaultPremiumPricingPlanMonthly": null,
        "defaultPremiumPricingPlanYearly": null,
        "defaultFreePricingPlanMonthly": null,
        "description": "Explainer for JIRA"
      },
      "sys": {
        "space": {
          "sys": {
            "type": "Link",
            "linkType": "Space",
            "id": "3s3v3nq72la0"
          }
        },
        "id": "7ljO65v3WeMZUEfL6Ng6NP",
        "type": "Entry",
        "createdAt": "2019-06-10T22:49:52.109Z",
        "updatedAt": "2019-07-09T12:38:41.966Z",
        "environment": {
          "sys": {
            "id": "stg",
            "type": "Link",
            "linkType": "Environment"
          }
        },
        "revision": 5,
        "contentType": {
          "sys": {
            "type": "Link",
            "linkType": "ContentType",
            "id": "product"
          }
        },
        "locale": "en-US"
      }
    }
  ],
  "includes": {
    "Entry": [
      {
        "sys": {
          "space": {
            "sys": {
              "type": "Link",
              "linkType": "Space",
              "id": "3s3v3nq72la0"
            }
          },
          "id": "9KKNU69Se5U58dv561jrE",
          "type": "Entry",
          "createdAt": "2019-07-01T02:24:18.118Z",
          "updatedAt": "2019-07-09T12:38:41.535Z",
          "environment": {
            "sys": {
              "id": "stg",
              "type": "Link",
              "linkType": "Environment"
            }
          },
          "revision": 3,
          "contentType": {
            "sys": {
              "type": "Link",
              "linkType": "ContentType",
              "id": "pricingPlan"
            }
          },
          "locale": "en-US"
        },
        "fields": {
          "status": "ACTIVE",
          "configuration": [
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 1140
                },
                {
                  "currency": "USD",
                  "unit_price": 10
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 0,
              "unit_limit_upper": 10
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 5560
                },
                {
                  "currency": "USD",
                  "unit_price": 49
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 11,
              "unit_limit_upper": 25
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 16900
                },
                {
                  "currency": "USD",
                  "unit_price": 149
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 26,
              "unit_limit_upper": 50
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 28300
                },
                {
                  "currency": "USD",
                  "unit_price": 249
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 51,
              "unit_limit_upper": 100
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 39600
                },
                {
                  "currency": "USD",
                  "unit_price": 349
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 101,
              "unit_limit_upper": 250
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 50900
                },
                {
                  "currency": "USD",
                  "unit_price": 449
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 251,
              "unit_limit_upper": 500
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 113000
                },
                {
                  "currency": "USD",
                  "unit_price": 990
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 501,
              "unit_limit_upper": 2000
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 226000
                },
                {
                  "currency": "USD",
                  "unit_price": 1990
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 2001,
              "unit_limit_upper": 10000
            },
            {
              "prices": [
                {
                  "currency": "JPY",
                  "unit_price": 339000
                },
                {
                  "currency": "USD",
                  "unit_price": 2990
                }
              ],
              "starter": false,
              "pricing_strategy": "FIXED_COST",
              "unit_description": "user",
              "unit_limit_lower": 10001,
              "unit_limit_upper": null
            }
          ],
          "pricingPlanId": "c77e96f3-e9f6-43ac-8d15-8b92f4c6451e",
          "sourceSequence": null,
          "source": "hams",
          "type": "Standard"
        }
      }
    ]
  }
};
