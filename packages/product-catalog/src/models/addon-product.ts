import { PricingPlans } from "./pricing-plan";

export type AddonProduct = {
    readonly name: string,
    readonly pricing: PricingPlans
}