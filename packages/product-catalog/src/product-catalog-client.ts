import { ContentfulClientApi, createClient, Entry } from 'contentful';
import { PricingPlans, ProductKey, ProductKeyMap } from './models';
import { AddonProduct } from './models/addon-product';
import {
  mapPricingPlanContentEntryToPricingPlan,
  PricingPlanContent,
  ProductContent
} from './product-catalog-client.types';

export type ProductCatalogClientOptions = {
  readonly space: string;
  readonly environment: string;
  readonly accessToken: string;
  readonly host?: string;
};

export const SELECT_QUERY = [
  'fields.key',
  'fields.defaultStdPricingPlanMonthly',
  'fields.defaultStdPricingPlanYearly',
  'fields.defaultPremiumPricingPlanMonthly',
  'fields.defaultPremiumPricingPlanYearly',
  'fields.defaultStdPricingPlanYearly',
  'fields.defaultFreePricingPlanMonthly',
  'fields.defaultFreePricingPlanMonthly'
].join(',');

export const SEARCH_ADDONS_QUERY = [SELECT_QUERY]
  .concat('fields.description')
  .join(',');

export interface IProductCatalogClient {
  getPricing({
    productKeys
  }: GetPricingOptions): Promise<ProductKeyMap<PricingPlans>>;
  searchAddons({
    parentProductKey,
    query,
    limit
  }: SearchAddonsOptions): Promise<AddonProduct[]>;
}

enum EDITION_SUFFIXES {
  FREE = '.free',
  STANDARD = '',
  PREMIUM = '.premium'
}

type EditionSuffix = { [P in keyof EDITION_SUFFIXES]: EDITION_SUFFIXES[P] };

const NON_STANDARD_EDITION_SUFFIXES = [
  EDITION_SUFFIXES.FREE,
  EDITION_SUFFIXES.PREMIUM
];

export const removeEditionSuffix = (productKey: string) =>
  productKey.replace(
    new RegExp(
      NON_STANDARD_EDITION_SUFFIXES.map(suffix => `\\${suffix}`).join('|') + '$'
    ),
    ''
  );

export const DEFAULT_HOST = "cdn-mr.contentful.com";

const getPricingPlansForEditionSuffixedKey = (
  productContent: ProductContent,
  editionSuffixedProductKey: string
): PricingPlans => {
  const matchedEditionSuffix: EditionSuffix = NON_STANDARD_EDITION_SUFFIXES.reduce(
    (matchingEditionSuffix, editionSuffix) =>
      editionSuffixedProductKey.endsWith(editionSuffix)
        ? editionSuffix
        : matchingEditionSuffix,
    EDITION_SUFFIXES.STANDARD
  );

  let monthly: Entry<PricingPlanContent> | null = null;
  let annual: Entry<PricingPlanContent> | null = null;

  switch (matchedEditionSuffix) {
    case EDITION_SUFFIXES.FREE:
      const {
        defaultFreePricingPlanMonthly,
        defaultFreePricingPlanYearly
      } = productContent;
      monthly = defaultFreePricingPlanMonthly;
      annual = defaultFreePricingPlanYearly;
      break;

    case EDITION_SUFFIXES.STANDARD:
      const {
        defaultStdPricingPlanMonthly,
        defaultStdPricingPlanYearly
      } = productContent;
      monthly = defaultStdPricingPlanMonthly;
      annual = defaultStdPricingPlanYearly;
      break;

    case EDITION_SUFFIXES.PREMIUM:
      const {
        defaultPremiumPricingPlanMonthly,
        defaultPremiumPricingPlanYearly
      } = productContent;
      monthly = defaultPremiumPricingPlanMonthly;
      annual = defaultPremiumPricingPlanYearly;
      break;
  }

  return {
    monthly: mapPricingPlanContentEntryToPricingPlan(monthly),
    annual: mapPricingPlanContentEntryToPricingPlan(annual)
  };
};

export class ProductCatalogClient implements IProductCatalogClient {
  private readonly contentfulClient: ContentfulClientApi;

  constructor(options: ProductCatalogClientOptions) {
    this.contentfulClient = createClient({ ...options, host: options.host || DEFAULT_HOST });
  }

  public async getPricing({
    productKeys
  }: GetPricingOptions): Promise<ProductKeyMap<PricingPlans>> {
    const { items } = await this.contentfulClient.getEntries<ProductContent>({
      content_type: 'product',
      select: SELECT_QUERY,
      'fields.key[in]': [...
        new Set(productKeys.map(removeEditionSuffix)).values()
      ].join(',')
    });
    
    let ret: ProductKeyMap<PricingPlans> = {};
    for (const productKey of productKeys) {
      const editionlessKey = removeEditionSuffix(productKey);
      const entry = items.find(({ fields: { key } }) => key === editionlessKey);
      if (entry) {
        ret = {
          ...ret,
          [productKey]: getPricingPlansForEditionSuffixedKey(
            entry.fields,
            productKey
          )
        };
      }
    }

    return ret;
  }

  public async searchAddons({
    parentProductKey,
    query,
    limit
  }: SearchAddonsOptions): Promise<AddonProduct[]> {
    const { items } = await this.contentfulClient.getEntries<ProductContent>({
      content_type: 'product',
      select: SEARCH_ADDONS_QUERY,
      'fields.productType[match]': 'ADDON',
      'fields.parentProductName[match]': parentProductKey,
      'fields.description[match]': query,
      limit
    });

    const ret = items.map(
      ({
        fields: {
          description,
          defaultStdPricingPlanMonthly,
          defaultStdPricingPlanYearly
        }
      }) => ({
        name: description,
        pricing: {
          monthly: mapPricingPlanContentEntryToPricingPlan(
            defaultStdPricingPlanMonthly
          ),
          annual: mapPricingPlanContentEntryToPricingPlan(
            defaultStdPricingPlanYearly
          )
        }
      })
    );

    return ret; 
  }
}

export type GetPricingOptions = {
  readonly productKeys: ReadonlyArray<ProductKey>;
};

export type SearchAddonsOptions = {
  readonly parentProductKey: ProductKey;
  readonly query: string;
  readonly limit?: number;
};
