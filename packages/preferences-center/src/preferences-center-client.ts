import { mapResponseToJson } from '@atlassian/asc-core';
import 'isomorphic-fetch';

export const AUDIENCE = 'atl-preferences-center';
export const PRODUCTION_BASE_URL =
  'https://atl-preferences-center.prod.atl-paas.net';
export const STAGING_BASE_URL =
  'https://atl-preferences-center.staging.atl-paas.net';
export const DEVELOPMENT_BASE_URL =
  'https://atl-preferences-center.dev.atl-paas.net';

export type AuthToken = string;

export type AuthTokenProviderContext = {
  readonly audience: typeof AUDIENCE;
};

export type AuthTokenProvider = (
  context: AuthTokenProviderContext
) => Promise<AuthToken>;

export type PreferencesCenterClientOptions = {
  readonly baseUrl: string;
  readonly authTokenProvider?: AuthTokenProvider;
};

export interface IPreferencesCenterClient {
  getConfig(request: GetConfigRequest): Promise<GetConfigResponse>;
  getUserConfig(request: GetUserConfigRequest): Promise<GetUserConfigResponse>;
  setUserConsent(request: SetUserConsentRequest): Promise<void>;
  updateUserConsent(request: UpdateUserConsentRequest): Promise<void>;
  setUserSubscriptions(request: SetUserSubscriptionsRequest): Promise<void>;
}

export class PreferencesCenterClient implements IPreferencesCenterClient {
  /**
   * Create a PreferencesCenterClient that will make requests to the Production environment.
   */
  static production() {
    return new PreferencesCenterClient({
      baseUrl: PRODUCTION_BASE_URL
    });
  }

  /**
   * Create a PreferencesCenterClient that will make requests to the Staging environment.
   */
  static staging() {
    return new PreferencesCenterClient({
      baseUrl: STAGING_BASE_URL
    });
  }

  /**
   * Create a PreferencesCenterClient that will make requests to the Development environment.
   */
  static development() {
    return new PreferencesCenterClient({
      baseUrl: DEVELOPMENT_BASE_URL
    });
  }
  private readonly baseUrl: string;
  private readonly authTokenProvider?: AuthTokenProvider;

  constructor({ baseUrl, authTokenProvider }: PreferencesCenterClientOptions) {
    this.baseUrl = baseUrl;
    this.authTokenProvider = authTokenProvider;
  }

  async getConfig({ locale }: GetConfigRequest): Promise<GetConfigResponse> {
    return mapResponseToJson(
      await fetch(`${this.baseUrl}/rest/forms/config?locale=${locale}`, {
        method: 'GET',
        headers: {
          Accept: 'application/json'
        }
      })
    );
  }

  async getUserConfig({
    email,
    locale
  }: GetUserConfigRequest): Promise<GetUserConfigResponse> {
    return mapResponseToJson(
      await fetch(`${this.baseUrl}/rest/forms/config`, {
        method: 'POST',
        body: JSON.stringify({
          email,
          locale
        }),
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
          Authorization: `Bearer ${await this.getAuthToken()}`
        }
      })
    );
  }

  async setUserConsent({
    email,
    site,
    locale,
    formUrl,
    providerId,
    consents,
    subscriptions
  }: SetUserConsentRequest): Promise<void> {
    await fetch(`${this.baseUrl}/rest/forms/consent`, {
      method: 'POST',
      body: JSON.stringify({
        email,
        site,
        locale,
        formUrl,
        providerId,
        consents,
        subscriptions
      }),
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${await this.getAuthToken()}`
      }
    });
  }

  async updateUserConsent({
    email,
    site,
    locale,
    formUrl,
    providerId,
    consents,
    subscriptions
  }: UpdateUserConsentRequest): Promise<void> {
    await fetch(`${this.baseUrl}/rest/forms/consent-update`, {
      method: 'POST',
      body: JSON.stringify({
        email,
        site,
        locale,
        formUrl,
        providerId,
        consents,
        subscriptions
      }),
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${await this.getAuthToken()}`
      }
    });
  }

  async setUserSubscriptions({
    email,
    site,
    locale,
    formUrl,
    providerId,
    consents,
    subscriptions,
    subscriberDetails
  }: SetUserSubscriptionsRequest): Promise<void> {
    await fetch(`${this.baseUrl}/rest/forms/subscriptions`, {
      method: 'POST',
      body: JSON.stringify({
        email,
        site,
        locale,
        formUrl,
        providerId,
        consents,
        subscriptions,
        subscriberDetails
      }),
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${await this.getAuthToken()}`
      }
    });
  }

  private async getAuthToken(): Promise<string> {
    if (this.authTokenProvider) {
      return this.authTokenProvider({ audience: AUDIENCE });
    } else {
      throw new Error('AuthTokenProvider is required to make this request.');
    }
  }
}

export type GetConfigRequest = {
  readonly locale: string;
};

export type GetUserConfigRequest = {
  readonly email: string;
  readonly locale: string;
};

export type GetConfigResponse = {
  readonly locale: string;
  readonly localeRequiresMarketingCommunicationOptIn: boolean;
};

export type GetUserConfigResponse = GetConfigResponse & {
  readonly hasUserMadeChoiceOnMarketingCommunicationOptIn: boolean;
  readonly usersChoiceOnMarketingCommunicationOptIn: boolean;
};

export type Consent = {
  readonly key: string;
  readonly displayedText: string;
  readonly granted: boolean;
};

export type Subscription = {
  readonly key: string;
  readonly subscribed: boolean;
};

export type SetUserConsentRequest = {
  readonly email: string;
  readonly site: string;
  readonly locale: string;
  readonly formUrl: string;
  readonly providerId?: string;
  readonly consents: Consent[];
  readonly subscriptions: Subscription[];
};

export type UpdateUserConsentRequest = SetUserConsentRequest;

export type SubscriberDetails = {
  readonly salutation?: string;
  readonly firstName?: string;
  readonly lastName?: string;
  readonly jobTitle?: string;
  readonly department?: string;
  readonly company?: string;
  readonly industry?: string;
  readonly yearsInBusiness?: string;
  readonly website?: string;
  readonly phone?: string;
  readonly fax?: string;
  readonly addressOne?: string;
  readonly addressTwo?: string;
  readonly city?: string;
  readonly state?: string;
  readonly territory?: string;
  readonly country?: string;
  readonly zip?: string;
};

export type SetUserSubscriptionsRequest = {
  readonly email: string;
  readonly site: string;
  readonly locale: string;
  readonly formUrl: string;
  readonly providerId?: string;
  readonly consents?: Consent[];
  readonly subscriptions: Subscription[];
  readonly subscriberDetails?: SubscriberDetails;
};
