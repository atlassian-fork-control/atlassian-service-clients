import { Cookie } from '@atlassian/asc-core';
import { Asap, UserProfile } from './models';

export type AsapAuth = {
  readonly asap: Asap;
};

export function isAsapAuth(auth: AsapAuth) {
  return !!(auth as AsapAuth).asap;
}

export type MeResponse = {
  readonly account_id: string;
  readonly name: string;
  readonly email: string;
  readonly picture: string;
  readonly locale: string;
  readonly zoneinfo: string;
};

export type MeOptions = {
  readonly cloudSessionTokenCookie?: Cookie;
};

export type PermissionsPermittedRequestAsap = {
  readonly auth: AsapAuth;
};

export type PermissionsPermittedRequestParams = {
  readonly dontRequirePrincipalInSite: boolean;
  readonly permissionId: string;
  readonly principalId: string;
  readonly resourceId: string;
};

export type PermissionsPermittedRequest = PermissionsPermittedRequestAsap &
  PermissionsPermittedRequestParams;

export type PermissionsPermittedResponse = string;

export type PatchUserProfileRequestBody = {
  readonly name?: string;
  readonly nickname: string;
};

export type PatchUserProfileRequest = {
  readonly cloudSessionTokenCookie: Cookie;
  readonly body: PatchUserProfileRequestBody;
  readonly userId: string;
};

export type PatchUserProfileResponse = {
  readonly account: {
    readonly account_id: string;
    readonly name: string;
    readonly nickname: string;
    readonly locale: string;
    readonly email: string;
    readonly picture: string;
    readonly account_type: string;
    readonly account_status: string;
    readonly email_verified: string;
  };
};

export function mapMeResponseToUserProfile({
  account_id,
  ...others
}: MeResponse): UserProfile {
  return {
    accountId: account_id,
    ...others
  };
}
