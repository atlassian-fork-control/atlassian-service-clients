import axios from 'axios';
import 'isomorphic-fetch';
import _ from 'lodash';
import { URL } from 'whatwg-url';

import {
  generateAsapToken,
  mapCookieToHeaders,
  mapResponseToText,
  serializeCookie,
  UnsucessfulFetchError
} from '@atlassian/asc-core';
import {
  isAsapAuth,
  mapMeResponseToUserProfile,
  MeOptions,
  PatchUserProfileRequest,
  PermissionsPermittedRequest,
  PermissionsPermittedRequestParams,
  PermissionsPermittedResponse
} from './identity-client.types';

import { UserProfile } from './models';

export const DEFAULT_STARGATE_BASE_URL =
  'https://api-private.stg.atlassian.com';
export const DEFAULT_IDENTITY_BASE_URL = 'https://id.staging.atl-paas.net';
const ASAP_AUDIENCE = 'identity-platform';

export interface IIdentityClient {
  me(options: MeOptions): Promise<UserProfile>;
}

export type IdentityClientOptions = {
  readonly baseUrlStargate?: string;
  readonly baseUrlIdentity?: string;
};

export class IdentityClient implements IIdentityClient {
  private readonly baseUrlStargate: string;
  private readonly baseUrlIdentity: string;

  constructor({
    baseUrlStargate = DEFAULT_STARGATE_BASE_URL,
    baseUrlIdentity = DEFAULT_IDENTITY_BASE_URL
  }: IdentityClientOptions = {}) {
    this.baseUrlStargate = baseUrlStargate;
    this.baseUrlIdentity = baseUrlIdentity;
  }

  async me({ cloudSessionTokenCookie }: MeOptions): Promise<UserProfile> {
    const url = `${this.baseUrlStargate}/me`;
    const response = await fetch(url, {
      headers: mapCookieToHeaders(cloudSessionTokenCookie)
    });
    if (response.ok) {
      return mapMeResponseToUserProfile(await response.json());
    } else {
      throw new UnsucessfulFetchError({
        url,
        status: response.status,
        message: response.statusText
      });
    }
  }

  async permissionsPermitted(
    request: PermissionsPermittedRequest
  ): Promise<PermissionsPermittedResponse> {
    const {
      auth,
      permissionId,
      principalId,
      resourceId,
      dontRequirePrincipalInSite
    } = request;

    const url = new URL(`${this.baseUrlIdentity}/v1/permissions/permitted`);
    const params: PermissionsPermittedRequestParams = {
      permissionId,
      principalId,
      resourceId,
      dontRequirePrincipalInSite
    };
    _.map(params, (val, key) => {
      url.searchParams.append(key, val.toString());
    });

    const headers = new Headers();

    if (isAsapAuth(auth)) {
      headers.append(
        'Authorization',
        `Bearer ${await generateAsapToken({
          ...auth.asap,
          audience: ASAP_AUDIENCE
        })}`
      );
    }

    return mapResponseToText(
      await fetch(`${url}`, {
        method: 'GET',
        headers
      })
    );
  }

  async patchUserProfile(request: PatchUserProfileRequest): Promise<any> {
    const { body, userId, cloudSessionTokenCookie } = request;

    const options = {
      headers: {
        'content-type': 'application/json',
        Cookie: serializeCookie(cloudSessionTokenCookie)
      }
    };
    return new Promise((resolve, reject) => {
      axios
        .patch(
          `${this.baseUrlStargate}/users/${userId}/manage/profile`,
          body,
          options
        )
        .then(response => {
          resolve(response.data);
        })
        .catch(err => {
          reject(err.toString());
        });
    });
  }
}
