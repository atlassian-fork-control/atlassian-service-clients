export type UserProfile = {
  readonly accountId: string;
  readonly name: string;
  readonly email: string;
  readonly picture: string;
  readonly locale: string;
  readonly zoneinfo: string;
};

