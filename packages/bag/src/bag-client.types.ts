import { Asap } from './models';

export type BagClientOptions = {
  readonly baseUrl?: string;
};

export type AsapAuth = {
  readonly asap: Asap;
};

export type BagRequestAuth = AsapAuth;

export function isAsapAuth(auth: BagRequestAuth): auth is AsapAuth {
  return !!(auth as AsapAuth).asap;
};

export type GetEntitlementGroupRequest = {
  readonly auth: BagRequestAuth;
  readonly entitlementGroupId: string;
};

export enum EditionTransitions {
  DOWNGRADE = "DOWNGRADE",
  UPGRADE = "UPGRADE"
};

export enum RenewalFrequency {
  MONTHLY = "MONTHLY",
  ANNUAL = "ANNUAL",
  NONE = "NONE"
}

export type MigrationEvaluation = {
  readonly usageLimit: number;
}

export type Entitlement = {
  readonly id: string;
  readonly sen: string;
  readonly productKey: string;
  readonly name: string;
  readonly entitlementGroupId: string;
  readonly accountId: string;
  readonly startDate: string;
  readonly endDate: string;
  readonly creationDate: string;
  readonly currentEdition?: string;
  readonly trialEdition?: string;
  readonly trialEditionEndDate?: string;
  readonly trialEndDate?: string;
  readonly futureEdition?: string;
  readonly editionTransitions: EditionTransitions[];
  readonly futureEditionTransitions?: EditionTransitions[];
  readonly status: string;
  readonly suspended: boolean;
  readonly entitlementMigrationEvaluation: MigrationEvaluation;
};

export type BillingPeriod = {
  readonly startDate: string;
  readonly endDate: string;
  readonly renewalFrequency: RenewalFrequency;
};

export type GetEntitlementGroupResponse = {
  readonly id: string;
  readonly name: string;
  readonly sen: string;
  readonly entitlements: Entitlement[];
  readonly accountId: string;
  readonly status: string;
  readonly suspended: boolean;
  readonly currentBillingPeriod: BillingPeriod;
  readonly nextBillingPeriod: BillingPeriod;
  readonly autoRenewed: boolean;
  readonly renewalAction: string;
  readonly license: string;
};
