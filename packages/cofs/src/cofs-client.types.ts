import { Cookie } from '@atlassian/asc-core';
import { Asap, XsrfToken } from './models';

export type Product = {
  readonly product: string;
  readonly accountName?: string;
};

export type ProductActivation = {
  readonly productKey: string;
  readonly edition?: string;
};

export type Experiment = {
  readonly experimentKey: string;
  readonly experimentAttribute: string;
};

export type AdvancedSettings = {
  readonly completionCallbackAudience?: string;
  readonly completionCallbackURL?: string;
  readonly originatorIP?: string;
  readonly suppressIdentityEmail?: boolean;
  readonly suppressEmail?: boolean;
  readonly evalDays?: number;
  readonly additionalProvisioningData?: any;
  readonly initialProductURL?: string;
};

export type ConsentItem = {
  readonly key: string;
  readonly displayedText: string;
  readonly granted: boolean;
};

export type Consent = {
  readonly site: string;
  readonly locale: string;
  readonly formUrl: string;
  readonly consents: ConsentItem[];
};

export type ActivateProductsRequestBody = {
  readonly products: ProductActivation[];
  readonly advancedSettings: { 
    additionalProvisioningData: { 
      actor: string,
      workspaceUuid: string 
    }
  };
};

export type CreateCloudRequestBody = {
  readonly cloudName: string;
  readonly email: string;
  readonly firstName: string;
  readonly lastName: string;
  readonly products: Product[];

  readonly adminPassword?: string;
  readonly adminUsername?: string;
  readonly country?: string;
  readonly state?: string;
  readonly timezone?: string;
  readonly coupon?: string;
  readonly experiment?: Experiment;
  readonly advancedSettings?: AdvancedSettings;
  readonly signupContext?: string;
  readonly testingPurposes?: boolean;
  readonly consent?: Consent;
  readonly languagePreference?: string;
};

export type AtlassianIdAuth = {
  readonly xsrfToken: XsrfToken;
  readonly cloudSessionTokenCookie: Cookie;
};

export type AsapAuth = {
  readonly asap: Asap;
};

export type RequestAuth = AtlassianIdAuth | AsapAuth;

export type CreateCloudRequestAnalyticsData = Cookie[];

type BaseRequest = {
  readonly activateProducts?: boolean;

  readonly source?: string;
  readonly clientIp?: string;
  readonly userContextToken?: string;
};

export type CreateCloudRequest = {
  readonly body: CreateCloudRequestBody;
  readonly auth: RequestAuth;

  readonly analyticsData?: CreateCloudRequestAnalyticsData;
} & BaseRequest;

export type ActivateProductsRequest = {
  readonly cloudId: string;
  readonly body: ActivateProductsRequestBody;
  readonly auth: RequestAuth;

  readonly analyticsData?: CreateCloudRequestAnalyticsData;
} & BaseRequest;

export function isAtlassianIdAuth(
  auth: RequestAuth
): auth is AtlassianIdAuth {
  const { xsrfToken, cloudSessionTokenCookie } = auth as AtlassianIdAuth;
  return !!xsrfToken && !!cloudSessionTokenCookie;
}

export function isAsapAuth(auth: RequestAuth): auth is AsapAuth {
  return !!(auth as AsapAuth).asap;
}

export type CreateCloudResponse = {
  readonly cloudId: string;
  readonly progressUri: string;
};

export type ActivateProductsResponse = {
  readonly progressUri: string;
};

export type GetNameAvailabilityOptions = {
  readonly name: string;
};

export type GetNameAvailabilityResponseBody = {
  readonly message: string;
  readonly success: boolean;
};

export type RenameSiteOptions = {
  readonly cloudId: string;
  readonly newSiteName: string;
};

export type RenameSiteResponseBody = {
  readonly progressUri: string;
};
