import * as core from '@atlassian/asc-core';
import fetchMock from 'fetch-mock';
import { BAD_REQUEST, OK } from 'http-status-codes';
import 'isomorphic-fetch';

import { CofsClient, STAGING_BASE_URL } from './index';

const baseUrl = 'http://base.url';
const xsrfToken = 'some-xsrf-token';
const cloudSessionTokenCookie = {
  name: 'cloud.session.token.stg',
  value: 'some-token'
};

const cloudAnalyticsData = [
  {
    name: 'ajs_anonymous_id',
    value: 'some-anonymous-id'
  },
  {
    name: '__atl_path',
    value: 'some-string'
  }
];

const asap = {
  issuer: 'some-issuer',
  keyId: 'some-key-id',
  privateKey: 'some-private-key'
};
const asapToken = 'some-asap-token';

const userContextToken = 'some-user-context-token';

describe('CofsClient', () => {
  describe('createCloud', () => {
    const requestBody = {
      cloudName: 'acme',
      firstName: 'Joe',
      lastName: 'Bloggs',
      email: 'joe.bloggs@somewhere.com',
      products: [{ product: 'jira-software.ondemand' }]
    };
    const responseBody = {
      cloudId: 'f6bc1bbc-01f6-45f7-a2fd-d7ef26d0f7ee',
      progressUri: 'http://some-progress.uri'
    };

    beforeEach(() =>
      jest.spyOn(core, 'generateAsapToken').mockResolvedValue(asapToken));

    afterEach(() => {
      jest.restoreAllMocks();
      fetchMock.restore();
    });

    it('should resolve with response body given OK status', async () => {
      const client = new CofsClient();

      fetchMock.post(`begin:${STAGING_BASE_URL}`, {
        status: OK,
        body: responseBody
      });

      await expect(
        client.createCloud({
          body: requestBody,
          auth: { xsrfToken, cloudSessionTokenCookie },
          analyticsData: cloudAnalyticsData,
        })
      ).resolves.toEqual(responseBody);

      const { method, body, headers } = fetchMock.lastOptions();
      const expectedCookies = cloudAnalyticsData
        .map(({ name, value }) => `${name}=${value}`)
        .join('; ');

      expect(method).toEqual('POST');
      expect(JSON.parse(body!.toString())).toEqual(requestBody);

      if (headers instanceof Headers) {
        expect(headers.get('Accept')).toEqual('application/json');
        expect(headers.get('Content-Type')).toEqual('application/json');
        expect(headers.get('X-ATL-Source')).toEqual(null);
        expect(headers.get('ATL-XSRF-Token')).toEqual(xsrfToken);
        expect(headers.get('Authorization')).toEqual(null);
        expect(headers.get('Cookie')).toEqual(expectedCookies);
      } else {
        expect(headers).toBeInstanceOf(Headers);
      }
    });

    it('should reject with error given BAD_REQUEST status', async () => {
      const client = new CofsClient({ baseUrl });

      fetchMock.post(`begin:${baseUrl}`, { status: BAD_REQUEST });

      await expect(
        client.createCloud({
          body: requestBody,
          auth: { xsrfToken, cloudSessionTokenCookie }
        })
      ).rejects.toMatchObject({
        url: `${baseUrl}/cloud`,
        status: BAD_REQUEST,
        message: 'Bad Request'
      });
    });

    it('should fetch with X-ATL-Source header given source option', async () => {
      const client = new CofsClient({ baseUrl });
      const source = 'some-source';

      fetchMock.post(`begin:${baseUrl}`, { status: OK, body: {} });

      await client.createCloud({
        body: requestBody,
        auth: { xsrfToken, cloudSessionTokenCookie },
        source
      });

      const { headers } = fetchMock.lastOptions();

      if (headers instanceof Headers) {
        expect(headers.get('X-ATL-Source')).toEqual(source);
      } else {
        expect(headers).toBeInstanceOf(Headers);
      }
    });

    it('should fetch with ATL-XSRF-Token header given xsrfToken option', async () => {
      const client = new CofsClient({ baseUrl });

      fetchMock.post(`begin:${baseUrl}`, { status: OK, body: {} });

      await client.createCloud({
        body: requestBody,
        auth: { xsrfToken, cloudSessionTokenCookie }
      });

      const { headers } = fetchMock.lastOptions();

      if (headers instanceof Headers) {
        expect(headers.get('ATL-XSRF-Token')).toEqual(xsrfToken);
      } else {
        expect(headers).toBeInstanceOf(Headers);
      }
    });

    it('should fetch with Authorization header given ASAP options', async () => {
      const client = new CofsClient({ baseUrl });

      fetchMock.post(`begin:${baseUrl}`, { status: OK, body: {} });

      await client.createCloud({
        body: requestBody,
        auth: { asap },
        userContextToken
      });

      expect(core.generateAsapToken).toBeCalledWith({
        audience: 'cofs',
        ...asap
      });

      const { headers } = fetchMock.lastOptions();
      if (headers instanceof Headers) {
        expect(headers.get('Authorization')).toEqual(`Bearer ${asapToken}`);
      } else {
        expect(headers).toBeInstanceOf(Headers);
      }
    });

    it('should make request with x-forward-for header given client ip', async () => {
      const client = new CofsClient({ baseUrl });
      const clientIp = '127.0.0.1';

      fetchMock.post(`begin:${baseUrl}`, { status: OK, body: responseBody });

      await expect(
        client.createCloud({
          body: requestBody,
          auth: { xsrfToken, cloudSessionTokenCookie },
          clientIp
        })
      ).resolves.toEqual(responseBody);

      const { headers } = fetchMock.lastOptions();

      if (headers instanceof Headers) {
        expect(headers.get('X-Forwarded-For')).toEqual(clientIp);
      } else {
        expect(headers).toBeInstanceOf(Headers);
      }
    });
  });

  describe('activateProducts', () => {
    const cloudId = 'f6bc1bbc-01f6-45f7-a2fd-d7ef26d0f7ee';
    const requestBody = {
      activateProducts: true,
      products: [
        { productKey: 'jira-software.ondemand', edition: 'standard' },
        { productKey: 'bitbucket.ondemand' }
      ],
      advancedSettings: {
        additionalProvisioningData: { 
          actor: 'test-actor',
          workspaceUuid: 'test-workspace-uuid' 
        }
      }
    };
    const responseBody = {
      progressUri: 'http://some-progress.uri'
    };

    beforeEach(() =>
      jest.spyOn(core, 'generateAsapToken').mockResolvedValue(asapToken));

    afterEach(() => {
      jest.restoreAllMocks();
      fetchMock.restore();
    });

    it('should resolve with response body given OK status', async () => {
      const client = new CofsClient();

      fetchMock.post(`begin:${STAGING_BASE_URL}`, {
        status: OK,
        body: responseBody
      });

      await expect(
        client.activateProducts({
          activateProducts: true,
          cloudId,
          body: requestBody,
          auth: { xsrfToken, cloudSessionTokenCookie },
          analyticsData: cloudAnalyticsData
        })
      ).resolves.toEqual(responseBody);

      const { method, body, headers } = fetchMock.lastOptions();
      const expectedCookies = cloudAnalyticsData
        .map(({ name, value }) => `${name}=${value}`)
        .join('; ');

      expect(method).toEqual('POST');
      expect(JSON.parse(body!.toString())).toEqual(requestBody);

      if (headers instanceof Headers) {
        expect(headers.get('Accept')).toEqual('application/json');
        expect(headers.get('Content-Type')).toEqual('application/json');
        expect(headers.get('ATL-XSRF-Token')).toEqual(xsrfToken);
        expect(headers.get('Authorization')).toEqual(null);
        expect(headers.get('Cookie')).toEqual(expectedCookies);
      } else {
        expect(headers).toBeInstanceOf(Headers);
      }
    });

    it('should reject with error given BAD_REQUEST status', async () => {
      const client = new CofsClient({ baseUrl });

      fetchMock.post(`begin:${baseUrl}`, { status: BAD_REQUEST });

      await expect(
        client.activateProducts({
          activateProducts: true,
          cloudId,
          body: requestBody,
          auth: { xsrfToken, cloudSessionTokenCookie }
        })
      ).rejects.toMatchObject({
        url: `${baseUrl}/cloud/${cloudId}/activateProducts`,
        status: BAD_REQUEST,
        message: 'Bad Request'
      });
    });

    it('should fetch with ATL-XSRF-Token header given xsrfToken option', async () => {
      const client = new CofsClient({ baseUrl });

      fetchMock.post(`begin:${baseUrl}`, { status: OK, body: {} });

      await client.activateProducts({
        activateProducts: true,
        cloudId,
        body: requestBody,
        auth: { xsrfToken, cloudSessionTokenCookie }
      });

      const { headers } = fetchMock.lastOptions();

      if (headers instanceof Headers) {
        expect(headers.get('ATL-XSRF-Token')).toEqual(xsrfToken);
      } else {
        expect(headers).toBeInstanceOf(Headers);
      }
    });

    it('should fetch with Authorization header given ASAP options', async () => {
      const client = new CofsClient({ baseUrl });

      fetchMock.post(`begin:${baseUrl}`, { status: OK, body: {} });

      await client.activateProducts({
        activateProducts: true,
        cloudId,
        body: requestBody,
        auth: { asap }
      });

      expect(core.generateAsapToken).toBeCalledWith({
        audience: 'cofs',
        ...asap
      });

      const { headers } = fetchMock.lastOptions();
      if (headers instanceof Headers) {
        expect(headers.get('Authorization')).toEqual(`Bearer ${asapToken}`);
      } else {
        expect(headers).toBeInstanceOf(Headers);
      }
    });
  });

  describe('getNameAvailability', () => {
    afterEach(() => {
      jest.restoreAllMocks();
      fetchMock.restore();
    });

    it('should resolve with true given OK status', async () => {
      const client = new CofsClient({ baseUrl });
      const name = 'some-cloud-name';

      fetchMock.post(`begin:${baseUrl}`, {
        status: OK,
        body: { success: true }
      });

      await expect(client.getNameAvailability({ name })).resolves.toEqual(true);

      const { method, body } = fetchMock.lastOptions();

      expect(method).toEqual('POST');
      expect(JSON.parse(body!.toString())).toEqual({ name });
    });
  });

  describe('renameSite', () => {
    afterEach(() => {
      jest.restoreAllMocks();
      fetchMock.restore();
    });

    it('should resolve with progressUri given OK status', async () => {
      const client = new CofsClient({ baseUrl });
      const newSiteName = 'some-new-site-name';
      const cloudId = 'some-cloud-id';

      fetchMock.put(`begin:${baseUrl}/${cloudId}/rename`, {
        status: OK,
        body: { progressUri: 'https://progress.uri.atlassian.com' }
      });

      await expect(
        client.renameSite({ newSiteName, cloudId })
      ).resolves.toEqual('https://progress.uri.atlassian.com');

      const { method, body } = fetchMock.lastOptions();

      expect(method).toEqual('PUT');
      expect(JSON.parse(body!.toString())).toEqual({ cloudName: newSiteName });
    });
  });
});
