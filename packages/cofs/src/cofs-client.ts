import {
  Cookie,
  generateAsapToken,
  mapResponseToJson,
  serializeCookie
} from '@atlassian/asc-core';
import {
  ActivateProductsRequest,
  ActivateProductsResponse,
  CreateCloudRequest,
  CreateCloudResponse,
  GetNameAvailabilityOptions,
  GetNameAvailabilityResponseBody,
  isAsapAuth,
  isAtlassianIdAuth,
  RenameSiteOptions,
  RenameSiteResponseBody
} from './cofs-client.types';

export const PROD_BASE_URL = 'https://cofs.prod.public.atl-paas.net';
export const STAGING_BASE_URL = 'https://cofs.staging.public.atl-paas.net';

const AUDIENCE = 'cofs';

export type CofsClientOptions = {
  readonly baseUrl?: string;
};

export class CofsClient {
  private readonly baseUrl: string;

  public constructor({ baseUrl = STAGING_BASE_URL }: CofsClientOptions = {}) {
    this.baseUrl = baseUrl;
  }

  public async activateProducts(
    request: ActivateProductsRequest
  ): Promise<ActivateProductsResponse> {
    const headers = new Headers({
      Accept: 'application/json',
      'Content-Type': 'application/json'
    });
    const { cloudId, body, auth, analyticsData } = request;

    let cookiesList: Cookie[] = [];
    if (analyticsData) {
      cookiesList = analyticsData;
    }

    if (isAtlassianIdAuth(auth)) {
      headers.append('ATL-XSRF-Token', auth.xsrfToken);
      cookiesList.push(auth.cloudSessionTokenCookie);
    } else if (isAsapAuth(auth)) {
      headers.append(
        'Authorization',
        `Bearer ${await generateAsapToken({
          ...auth.asap,
          audience: AUDIENCE
        })}`
      );
    }

    if (cookiesList.length) {
      headers.append(
        'Cookie',
        cookiesList.map(cookieItem => serializeCookie(cookieItem)).join('; ')
      );
    }

    return mapResponseToJson(
      await fetch(`${this.baseUrl}/cloud/${cloudId}/activateProducts`, {
        method: 'POST',
        headers,
        body: JSON.stringify(body)
      })
    );
  }

  public async createCloud(
    originalRequest: CreateCloudRequest
  ): Promise<CreateCloudResponse> {
    const request = originalRequest as CreateCloudRequest;
    const headers = new Headers({
      Accept: 'application/json',
      'Content-Type': 'application/json'
    });
    const {
      body,
      auth,
      userContextToken,
      source,
      clientIp,
      analyticsData
    } = request;

    if (source) {
      headers.append('X-ATL-Source', source);
    }

    let cookiesList: Cookie[] = [];
    if (analyticsData) {
      cookiesList = analyticsData;
    }

    if (isAtlassianIdAuth(auth)) {
      headers.append('ATL-XSRF-Token', auth.xsrfToken);
      cookiesList.push(auth.cloudSessionTokenCookie);
    } else if (isAsapAuth(auth)) {
      headers.append(
        'Authorization',
        `Bearer ${await generateAsapToken({
          ...auth.asap,
          audience: AUDIENCE
        })}`
      );

      if (userContextToken) {
        headers.append('User-Context', userContextToken);
      }
    }

    if (cookiesList.length) {
      headers.append(
        'Cookie',
        cookiesList.map(cookieItem => serializeCookie(cookieItem)).join('; ')
      );
    }

    if (clientIp) {
      headers.append('X-Forwarded-For', clientIp);
    }

    return mapResponseToJson(
      await fetch(`${this.baseUrl}/cloud`, {
        method: 'POST',
        headers,
        body: JSON.stringify(body)
      })
    );
  }

  public async getNameAvailability({
    name
  }: GetNameAvailabilityOptions): Promise<boolean> {
    const { success } = await mapResponseToJson<
      GetNameAvailabilityResponseBody
    >(
      await fetch(`${this.baseUrl}/nameAvailability`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({
          name
        })
      })
    );

    return success;
  }

  public async renameSite({
    cloudId,
    newSiteName
  }: RenameSiteOptions): Promise<string> {
    const { progressUri } = await mapResponseToJson<RenameSiteResponseBody>(
      await fetch(`${this.baseUrl}/${cloudId}/rename`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json'
        },
        credentials: 'include',
        body: JSON.stringify({ cloudName: newSiteName })
      })
    );
    return progressUri;
  }
}
