export type Cookie = {
  readonly name: string;
  readonly value: string;
};

