import { Cookie } from './cookie';

export type GetXsrfTokenRequest = {
  readonly cloudSessionTokenCookie: Cookie;
};

export type GetEmailExistsRequest = {
  readonly email: string;
};

export type GetEmailExistsResponse = {
  readonly message: string;
  readonly success: boolean;
};

export type GetCartRequest = {
  readonly cartId: string;
};

export type PatchUserRequest = {
  readonly cloudSessionTokenCookie: Cookie;
  readonly firstName: string;
  readonly lastName: string;
};
