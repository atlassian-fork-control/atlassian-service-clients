import { isObject } from 'util';

export function isRecord(
  headers?: HeadersInit
): headers is Record<string, string> {
  return isObject(headers);
}
