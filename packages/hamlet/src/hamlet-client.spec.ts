import { UnsucessfulFetchError } from '@atlassian/asc-core';
import fetchMock from 'fetch-mock';

import { cleanupFixtures, setupFixtures } from './fixtures';
import { HamletClient } from './hamlet-client';
import { serializeCookie } from './utils';

describe('HamletClient', () => {
  const baseUrl = 'http://some.url';

  describe('getXsrfToken', () => {
    it('should make request with cloud session token cookie', async () => {
      const { cloudSessionTokenCookie, xsrfToken } = setupFixtures({ baseUrl });
      const client = new HamletClient({ baseUrl });

      await expect(
        client.getXsrfToken({
          cloudSessionTokenCookie
        })
      ).resolves.toEqual(xsrfToken);
    });

    it('should reject with UnsuccessfulFetchError given invalid cloud session token cookie', async () => {
      setupFixtures({ baseUrl });
      const client = new HamletClient({ baseUrl });

      await expect(
        client.getXsrfToken({
          cloudSessionTokenCookie: {
            name: 'some-invalid-name',
            value: 'some-invalid-value'
          }
        })
      ).rejects.toThrow(UnsucessfulFetchError);
    });
  });

  describe('getEmailExists', () => {
    it('should respond with message and success is true given email that exists', async () => {
      setupFixtures({ baseUrl });
      const client = new HamletClient({
        baseUrl
      });

      await expect(
        client.getEmailExists({ email: 'existing-email@somewhere.com' })
      ).resolves.toEqual({
        message: 'An Atlassian account exists for this email address',
        success: true
      });
    });

    it('should respond with no message and success if false given email that does not exist', async () => {
      setupFixtures({ baseUrl });
      const client = new HamletClient({
        baseUrl
      });

      await expect(
        client.getEmailExists({ email: 'non-existant-email@somewhere.com' })
      ).resolves.toEqual({
        message: null,
        success: false
      });
    });
  });

  describe('getCart', () => {
    it('should respond with data from hamlet', async () => {
      const client = new HamletClient({
        baseUrl
      });
      const cartId = 'STRONG_UUID-37f459a3-dd97-4c06-9644-e2c6eb2cb6ae';

      fetchMock.get(`${baseUrl}/1.0/public/order/cart/${cartId}`, {
        status: 200,
        body: JSON.stringify({ data: 'some-data' })
      });

      await expect(client.getCart({ cartId })).resolves.toEqual({
        data: 'some-data'
      });
    });
  });

  describe('patchUser', () => {
    it('should respond with data from hamlet', async () => {
      const { cloudSessionTokenCookie } = setupFixtures({ baseUrl });
      const client = new HamletClient({
        baseUrl
      });
      await expect(
        client.patchUser({
          cloudSessionTokenCookie,
          firstName: 'firstName',
          lastName: 'lastName'
        })
      ).resolves.toEqual({
        contactDetails: {
          firstName: 'John',
          lastName: 'Wick',
          email: 'hireakiller@shhh.com',
          phone: '323-234-2352'
        }
      });
    });

    it('should fetch with ATL-XSRF-Token header given xsrfToken option', async () => {
      const { cloudSessionTokenCookie, xsrfToken } = setupFixtures({ baseUrl });
      const client = new HamletClient({
        baseUrl
      });
      await expect(
        client.patchUser({
          cloudSessionTokenCookie,
          firstName: 'firstName',
          lastName: 'lastName'
        })
      ).resolves.toEqual({
        contactDetails: {
          firstName: 'John',
          lastName: 'Wick',
          email: 'hireakiller@shhh.com',
          phone: '323-234-2352'
        }
      });

      const { headers } = fetchMock.lastOptions();

      if (headers instanceof Headers) {
        expect(headers.get('ATL-XSRF-Token')).toEqual(xsrfToken);
        expect(headers.get('Cookie')).toEqual(
          serializeCookie(cloudSessionTokenCookie)
        );
      } else {
        expect(headers).toBeInstanceOf(Headers);
      }
    });
  });

  afterEach(() => cleanupFixtures());
});
