export type Asap = {
  readonly keyId: string;
  readonly privateKey: string;
};

export type GenerateAsapTokenOptions = {
  readonly payload?: string | object;
  /** PEM encoded private key */
  readonly privateKey: string;
  readonly keyId: string;
  readonly expiresIn?: number | string;
  readonly audience: string;
  readonly issuer: string;
  readonly subject?: string;
};


export type DecodeAsapTokenOptions = {
  readonly iss?: string;
  readonly aud?: string;
  readonly nbf?: number;
  readonly exp?: number;
  readonly iat?: number;
  readonly jti?: string;
  readonly sub?: string;
  readonly email?: string;
  readonly scope?: string;
  readonly verified?: boolean;
  readonly sessionId?: string;
  readonly accountId?: string;
  readonly emailDomain?: string;
  readonly firstParty?: boolean;
  readonly onBehalfOf?: boolean;
  readonly oauthClientId?: string;
  readonly refreshTimeout?: number;
  readonly systemAccountId?: string;
  readonly requestPrincipal?: string;
  readonly customDomainRequest?: boolean;
  readonly domains?: any[];
  readonly associations?: any[];
  readonly impersonation?: any[];
};
