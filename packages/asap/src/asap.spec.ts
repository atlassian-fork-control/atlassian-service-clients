import { 
  decodeAsapPrivateKeyDataUri,
  decodeAsapToken,
  DEFAULT_EXPIRES_IN_SECONDS,
  generateAsapToken
} from './index';

describe('ASAP', () => {
  const dataUri =
    'data:application/pkcs8;kid=micros%2Fbxp-express%2Fjluong-8m7e1ddgfd38gqs8;base64,MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDaqFosDL02ePCWM4UzIrGqMUIdgDmKycuW+VK5oRVse/zLOvHX1G4bGYvPbU9+RfAwoQ3R0W0511ImT9mhTf58iINnK5qbJ2aKcX1eAPU9F7AdU977pVe3c5fDDciXlcWhjjJ7ekW7HKqL0+l6+0jMGYcD55tSfZ6Jcm6auVAZDxg8NKlVkJJ6VuqxsLaW4D3SsouvaD4qVZoyovrDLJLHgcQbnFwMGr4RkXElm0e8zZqvKTd+owUtrMbB0HqgsyyS/OCIWoBmIV6ZjHJaDcc/D2u/nqA64fB28S+twkRhK2eEG/ovk9RE7qfheUGW/iBK5SHHOBhTDR9hO+pmhL2dAgMBAAECggEADt+o0ppDPLNZWj5C70mK4FLddnhEccZsTObE08+7T1fXyFNIJDBVFQ7VzdRXtfU383bhI8alMWwOMID6Atj1WK9IkPoMWEWD2iMCLiTOictUtYoA/wkuQFJinmgPnxAXE3hCwtd8as/2YfCWT0FMoGWUI5XboO7em+bm0yuL4UhfU7Jb+P+OAsNQEc3gZZZRun7+E0PXR9tttG8COB/In78GHslvQCvZ5VIe3tny6CKnzlwPNO3m+qRUg6di/9m9tgFiiqwacwlYZN26iHY4D1Lq+sR8QT5r2N0Wcj3z3BoFF9oSDXpZPSigT4g/zK8wYMJEOVSQFuKukkD2eSe/AQKBgQD7vNOb0aLu4gmk04+NU0TdRSUBArFySHHVNxWlIcqqG/PUZcR6HoTIy5u78SMO27TzCbS9bykKePO2Poy55miLmeHuXvbDVedkEv5kepjINOcMY6MjiL07meYgXowhDEHr4cn1uPVgK8t62+WEQT5gxdYY/T4Z0dBRfpAVfb5PoQKBgQDeXCNlwb6biYZrls+bOc24eyKy23ZzAYwqDuu0Fi83mVBFDfVlkYcjfYvOL7a23VTW5/Xv9E7qexunbRq2x5JR+JU6vlAI6eDjUFa7K48m6zT9EObBZDyIV8OMTezbqZAtFZAKsPCHpqPWh+Y9dCCeymQU3xBO7mpQ4A5yIbpcfQKBgQCytalPu/I8PfF0ts/5Fz0TDCwp3HyWUgGlyLYwBZ43RyLBDQH/Y70GQy0qrhQiTyzVQHWz6b8r9ROmtB75Ni3PtQ2kSHxLzac37xfzwujFcHD55Z4B8ufL8QRixFwcdfQyfZb/Py/K9yw76vG0AxFCaBAGq3kzoTSPNhcZeqvpIQKBgQCnUgfk0zR9z6+RmAsikA2IS0gh9FieU8TIBTBB9AuVt80aGVXWFvrsrtUb0vWXhtcHW8bJyDn14as+gi/+6A/nlLmeWYTaIDt0cYjCERYIQCdAYo5xad7O7dOc6SpZZK+z7SzVjq2ANdnFXb/X8GF3e25x8iRQcMEZmG/mXBDi9QKBgQCjRdTXVj6KZi2OyFRVG6iw/GJ74wl3sbixwY6tHNYJxwgMxcqG4NWgna+70+UUMyLMofmi0fqP3jBeeQlwHVliIkPXsPsJhiotnSVMKiqkhOl1mRsqDa/82q0kNoTRwo3s26tG69IHf2Yo3ow9NTNcaGP5OkkrZJDH+mkQ8QL5og==';

  const nowSeconds = 1000000;
  const issuer = 'some-issuer';
  const subject = 'some-subject';
  const audience = 'some-audience';

  describe('generateAsapToken', () => {
    beforeEach(() =>
      jest.spyOn(Date, 'now').mockReturnValue(nowSeconds * 1000));

    afterEach(() => jest.restoreAllMocks());

    it('return a jwt token', () => {
      const { keyId, privateKey } = decodeAsapPrivateKeyDataUri(dataUri);
      const token = generateAsapToken({
        issuer,
        subject,
        audience,
        privateKey,
        keyId
      });

      expect(decodeAsapToken(token)).toEqual({
        aud: audience,
        iat: nowSeconds,
        exp: nowSeconds + DEFAULT_EXPIRES_IN_SECONDS,
        iss: issuer,
        sub: subject,
        jti: expect.any(String)
      });
    });

    it('default subject to issuer given subject is undefined', () => {
      const { keyId, privateKey } = decodeAsapPrivateKeyDataUri(dataUri);
      const token = generateAsapToken({
        issuer,
        audience,
        privateKey,
        keyId
      });

      expect(decodeAsapToken(token)).toEqual({
        aud: audience,
        iat: nowSeconds,
        exp: nowSeconds + DEFAULT_EXPIRES_IN_SECONDS,
        iss: issuer,
        sub: issuer,
        jti: expect.any(String)
      });
    });

    it('throws error if data URI is invalid', () => {
      const dataUriInvalid = 'data:solicitud/pkcs8;base64,test';
      expect(() => {
        decodeAsapPrivateKeyDataUri(dataUriInvalid)
      }).toThrow(Error('Data URI does not match ASAP_PRIVATE_KEY format.'));
    });
  });
});
