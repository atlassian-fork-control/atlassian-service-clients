import { decode } from 'jsonwebtoken';
import * as forge from 'node-forge';
import { Asap, DecodeAsapTokenOptions } from './asap.types'

const DATA_URI_PATTERN = /^data:application\/pkcs8;kid=([\w.\-+/]+);base64,([a-zA-Z0-9+/=]+)$/;

function decodePrivateKeyBase64(base64: string): forge.pki.PEM {
  const der = forge.util.decode64(base64);
  const asn1 = forge.asn1.fromDer(der);
  const privateKey = (forge.pki as any).privateKeyFromAsn1(asn1);
  return forge.pki.privateKeyToPem(privateKey);
}

export function decodeAsapPrivateKeyDataUri(dataUri: string): Asap {
  const match = DATA_URI_PATTERN.exec(decodeURIComponent(dataUri));
  if (match) {
    return {
      keyId: match[1],
      privateKey: decodePrivateKeyBase64(match[2])
        .replace(/\\n/g, '\n')
        .replace(/"/g, '')
        .trim()
    };
  } else {
    throw new Error('Data URI does not match ASAP_PRIVATE_KEY format.');
  }
}

export function decodeAsapToken(
  token: string
): DecodeAsapTokenOptions {
  let decodedToken = decode(token) || {};
  if (typeof decodedToken === 'string') { decodedToken = {}; }
  return decodedToken;
}
