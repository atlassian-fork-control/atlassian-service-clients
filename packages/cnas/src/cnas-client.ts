import { mapResponseToJson } from '@atlassian/asc-core';

export const STAGING_BASE_URL =
  'https://wac.stg.internal.atlassian.com/apis/stg/cnas';
export const PRODUCTION_BASE_URL = 'https://www.atlassian.com/apis/prod/cnas';

export type CnasClientOptions = {
  readonly baseUrl?: string;
};

export interface ICnasClient {
  getNameAvailability: (
    { cloudName }: GetNameAvailabilityRequest
  ) => Promise<GetNameAvailabilityResponse>;
}

export class CnasClient implements ICnasClient {
  private readonly baseUrl: string;

  constructor({ baseUrl = STAGING_BASE_URL }: CnasClientOptions = {}) {
    this.baseUrl = baseUrl;
  }

  async getNameAvailability({
    cloudName
  }: GetNameAvailabilityRequest): Promise<GetNameAvailabilityResponse> {
    return mapResponseToJson(
      await fetch(`${this.baseUrl}/shopping-cart/name-availability/queries`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          cloudName
        })
      })
    );
  }
}

export type GetNameAvailabilityRequest = {
  readonly cloudName: string;
};

export type GetNameAvailabilityResponse = {
  readonly availability: string;
  readonly details: string;
  readonly hasErrors: boolean;
};
