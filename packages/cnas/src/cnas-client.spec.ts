import { UnsucessfulFetchError } from '@atlassian/asc-core';
import 'isomorphic-fetch';

import { CnasClient } from './cnas-client';
import { cleanupFixtures, setupFixtures } from './fixtures';

describe('CnasClient', () => {
  const baseUrl = 'http://some.url';

  describe('getNameAvailability', () => {
    it('should resolve with available given name is available to use', async () => {
      setupFixtures({ baseUrl });
      const client = new CnasClient({ baseUrl });

      await expect(
        client.getNameAvailability({
          cloudName: 'non-existing-name'
        })
      ).resolves.toEqual({
        availability: 'AVAILABLE',
        details: 'Name is available for use.',
        hasErrors: false
      });
    });

    it('should resolve with taken given name is not available to use', async () => {
      setupFixtures({ baseUrl });
      const client = new CnasClient({ baseUrl });

      await expect(
        client.getNameAvailability({
          cloudName: 'existing-name'
        })
      ).resolves.toEqual({
        availability: 'TAKEN',
        details: 'Name has been taken.',
        hasErrors: false
      });
    });

    it('should resolve with invalid given name is invalid', async () => {
      setupFixtures({ baseUrl });
      const client = new CnasClient({ baseUrl });

      await expect(
        client.getNameAvailability({
          cloudName: '-invalid-name-'
        })
      ).resolves.toEqual({
        availability: 'INVALID',
        details: 'Name is invalid.',
        hasErrors: false
      });
    });

    it('should reject with UnsuccessfulFetchError given no cloud name', async () => {
      setupFixtures({ baseUrl });
      const client = new CnasClient({ baseUrl });

      await expect(
        client.getNameAvailability({
          cloudName: undefined as any
        })
      ).rejects.toThrow(UnsucessfulFetchError);
    });
  });

  afterEach(() => cleanupFixtures());
});
