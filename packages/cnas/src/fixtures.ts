import fetchMock, { MockRequest } from 'fetch-mock';
import { isString } from 'util';

export type SetupFixturesOptions = {
    readonly baseUrl: string;
  };
  
  export function setupFixtures({ baseUrl }: SetupFixturesOptions) {
    fetchMock
      .post(
        (url, request) => {
          return (
            url === `${baseUrl}/shopping-cart/name-availability/queries` &&
            hasCloudName('non-existing-name')(request)
          );
        },
        {
          status: 200,
          body: {
            availability: 'AVAILABLE',
            details: 'Name is available for use.',
            hasErrors: false
          }
        }
      )
      .post(
        (url, request) => {
          return (
            url === `${baseUrl}/shopping-cart/name-availability/queries` &&
            hasCloudName('existing-name')(request)
          );
        },
        {
          status: 200,
          body: {
            availability: 'TAKEN',
            details: 'Name has been taken.',
            hasErrors: false
          }
        }
      )
      .post(
        (url, request) => {
          return (
            url === `${baseUrl}/shopping-cart/name-availability/queries` &&
            hasCloudName('-invalid-name-')(request)
          );
        },
        {
          status: 200,
          body: {
            availability: 'INVALID',
            details: 'Name is invalid.',
            hasErrors: false
          }
        }
      )
      .post(
        url => {
          return url === `${baseUrl}/shopping-cart/name-availability/queries`;
        },
        {
          status: 400
        }
      );
  }
  
  export function cleanupFixtures() {
    return fetchMock.restore();
  }
  
  function hasCloudName(expected: string) {
    return ({ body }: MockRequest) => {
      if (isString(body)) {
        const { cloudName } = JSON.parse(body);
        return cloudName === expected;
      } else {
        return false;
      }
    };
  }
  