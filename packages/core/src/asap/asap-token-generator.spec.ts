import { decode } from 'jsonwebtoken';
import NodeRSA from 'node-rsa';

import { generateAsapToken } from './asap-token-generator';

describe('generateAsapToken', () => {
  it('should generate a token that is decodable', async () => {
    const rsa = new NodeRSA({ b: 512 });
    const token = await generateAsapToken({
      issuer: 'some-issuer',
      keyId: 'some-key-id',
      privateKey: rsa.exportKey('private'),
      audience: 'some-audience',
      subject: 'some-subject'
    });

    expect(decode(token)).toEqual({
      iss: 'some-issuer',
      sub: 'some-subject',
      aud: 'some-audience',
      iat: expect.any(Number),
      exp: expect.any(Number),
      jti: expect.any(String)
    });
  });

  it('subject should equal issuer if subject is not received for token encoding', async () => {
    const rsa = new NodeRSA({ b: 512 });
    const token = await generateAsapToken({
      issuer: 'some-issuer',
      keyId: 'some-key-id',
      privateKey: rsa.exportKey('private'),
      audience: 'some-audience'
    });

    expect(decode(token)).toEqual({
      iss: 'some-issuer',
      sub: 'some-issuer',
      aud: 'some-audience',
      iat: expect.any(Number),
      exp: expect.any(Number),
      jti: expect.any(String)
    });
  });
});
