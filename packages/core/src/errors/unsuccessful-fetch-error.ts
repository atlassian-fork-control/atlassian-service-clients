export type UnsucessfulFetchErrorOptions = {
  readonly url: string;
  readonly status: number;

  readonly message?: string;
  readonly response?: any;
};

export class UnsucessfulFetchError extends Error {
  public readonly url: string;
  public readonly status: number;
  public readonly response: any;

  constructor({ url, status, message, response }: UnsucessfulFetchErrorOptions) {
    super(message);
    this.name = UnsucessfulFetchError.name;
    this.url = url;
    this.status = status;
    this.response = response;
  }
}

export function mapResponseToUnsucessfulFetchError(response: Response) {

  return new UnsucessfulFetchError({
      url: response.url,
      status: response.status,
      message: response.statusText,
      response,
    });
} 